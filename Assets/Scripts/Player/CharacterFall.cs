using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterFall : MonoBehaviour
{
    [SerializeField] private Rigidbody[] m_Bodies;
    [SerializeField] private float m_ImpactForce;
    [SerializeField] private Transform m_ImpactPoint;
    private Vector3 m_InitialPosBody, m_InitialPosCycle;
    [SerializeField] private ParticleSystem m_Explosion;
    // Start is called before the first frame update
    void Start()
    {
        m_InitialPosBody = m_Bodies[0].transform.localPosition;
        m_InitialPosCycle= m_Bodies[1].transform.localPosition;
    }
    private void OnEnable()
    {
        resetBodies();
        explodePlayer();
    }

    void resetBodies()
    {
        m_Bodies[0].transform.localPosition = m_InitialPosBody;
        m_Bodies[1].transform.localPosition = m_InitialPosCycle;
        foreach (Rigidbody bodies in m_Bodies)
        {
            bodies.angularVelocity = Vector3.zero;
            bodies.velocity = Vector3.zero;
        }
    }

    void explodePlayer()
    {
        foreach(Rigidbody bodies in m_Bodies)
        {
            bodies.AddExplosionForce(m_ImpactForce,m_ImpactPoint.position,2);
        }
        m_Explosion.Play();
    }

}
