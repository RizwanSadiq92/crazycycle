﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowerResetScript : MonoBehaviour
{
    #region Data
    [SerializeField] private GameObject m_Follower;
    [SerializeField] private BoxCollider m_FollowerCollider;
    #endregion Data
    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += ResetFollower;
    }
    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= ResetFollower;
    }

    private void ResetFollower()
    {
        m_Follower.SetActive(true);
        m_FollowerCollider.enabled = true;
    }


    #region Collision
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(nameof(eTagType.Player)))
        {
            m_Follower.SetActive(false);
            m_FollowerCollider.enabled = false;
            Player.Instance.AddFollowPlayer();
        }
    }

    #endregion Collision
}
