﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    #region Data
    public GameObject m_PlayerToFollow;
    [SerializeField] private float m_FollowSpeed;
    public bool isFollowing;

    public Vector3 Offset;
    #endregion Data


    void Update()
    {
        if (isFollowing)
        {
            transform.position = Vector3.Lerp(transform.position, m_PlayerToFollow.transform.position + Offset, m_FollowSpeed * Time.deltaTime);
            transform.LookAt(m_PlayerToFollow.transform);
        }
    }
}
