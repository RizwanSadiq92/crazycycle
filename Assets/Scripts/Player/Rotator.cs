using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    // Start is called before the first frame update
    #region Data
    public float m_speed;
    
    #endregion Data

    void Update()
    {
        if(Gamemanager.Instance.e_gamestates == eGameStates.Playing)
            transform.Rotate(-Time.deltaTime * 90*m_speed, 0, 0);
    }
}
