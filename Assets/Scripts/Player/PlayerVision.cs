using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVision : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float m_DistanceDetect;
    [SerializeField] Transform RayCastPoint;
    [SerializeField] private float m_SphereRadius;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Gamemanager.Instance.e_gamestates == eGameStates.Playing)
        {

            if (Physics.SphereCast(RayCastPoint.transform.position, m_SphereRadius, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
            {
                Debug.DrawRay(RayCastPoint.transform.position , transform.TransformDirection(Vector3.down) * m_DistanceDetect, Color.green);
                //CheckFail(hit);
            }
            else
            {
                Debug.DrawRay(RayCastPoint.transform.position, transform.TransformDirection(Vector3.down) * m_DistanceDetect, Color.red);
                GameFail();
            }

        }
    }

    void GameFail()
    {
        StartCoroutine(I_GameFail());
    }

    IEnumerator I_GameFail()
    {
        yield return new WaitForSeconds(0.2f);
        Gamemanager.Instance.GameFail();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        RaycastHit hit;
        var isHit = Physics.SphereCast(RayCastPoint.transform.position, m_SphereRadius, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity);
        if (isHit)
            Gizmos.DrawWireSphere(hit.point, m_SphereRadius);
    }
}
