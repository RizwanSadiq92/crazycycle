using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region Data

    public static Player Instance;

    [SerializeField] private PlayerMovement m_PlayerScript;

    [SerializeField] private List<FollowPlayer> m_FollowingPlayers;
    [SerializeField] private int m_StartingFollowersCount;
    public int StartingFollowersCount { get { return m_StartingFollowersCount; } set { m_StartingFollowersCount = value; } }
    [SerializeField] private float m_FollowingPlayerOffset;
    [SerializeField] private SplineFollower m_PlayerSpline;
    [SerializeField] private Rigidbody m_RigidBodyPlayer;
    [SerializeField] private BoxCollider m_ColliderPlayer;
    public int m_CurrentFollowersCount;

    [SerializeField] private GameObject m_ExplodingBody;
    private SplineComputer m_CurrentSpline;

    #endregion Data


    #region Unity Initialization
    private void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {

            Destroy(this.gameObject);
        }


        DontDestroyOnLoad(this);

    }

    private void OnEnable()
    {
        GameManagerDelegate.OnGameStart += OnGameStart;
        GameManagerDelegate.OnGameFail += OnGameFail;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameStart -= OnGameStart;
        GameManagerDelegate.OnGameFail -= OnGameFail;
    }

    #endregion Unity Initialization


    public void SetPlayer(SplineComputer i_SplineComputer)
    {
        m_CurrentSpline = i_SplineComputer;
        m_PlayerScript.SetPlayerSpline(i_SplineComputer);
        resetPlayerValues();
    }

    public void SetFollowersCount(int FollowerCount)
    {
        StartingFollowersCount = FollowerCount;
    }

    void resetPlayerValues()
    {
        m_RigidBodyPlayer.gameObject.SetActive(true);
        m_RigidBodyPlayer.isKinematic = true;
        m_PlayerSpline.follow = false;
        m_PlayerSpline.RebuildImmediate();
        m_PlayerSpline.Restart(0);
        m_PlayerSpline.SetDistance(0);
        m_PlayerScript.m_BicycleObject.transform.localPosition = Vector3.zero;
        m_RigidBodyPlayer.transform.localPosition = Vector3.zero;
        m_RigidBodyPlayer.transform.localEulerAngles = Vector3.zero;
        StartingFollowPlayers();
    }

    #region FollowingPlayers
    void StartingFollowPlayers()
    {
        ClosingAllFollowPlayers();
        for (int i = 0; i < m_StartingFollowersCount; i++)
        {
            if (m_FollowingPlayers[i].gameObject.activeSelf == false)
            {
                var position = m_FollowingPlayers[i].m_PlayerToFollow.transform.position;
                m_FollowingPlayers[i].transform.position = position + new Vector3(0, 0, m_FollowingPlayerOffset);
                m_FollowingPlayers[i].transform.rotation = this.transform.rotation;
                m_FollowingPlayers[i].gameObject.SetActive(true);
            }
        }
        m_CurrentFollowersCount = m_StartingFollowersCount;
        HUDManager.Instance.AddCyclist(m_CurrentFollowersCount);
    }

    void ClosingAllFollowPlayers()
    {
        for (int i = 0; i < m_FollowingPlayers.Count; i++)
        {
            if (m_FollowingPlayers[i].gameObject.activeSelf == true)
            {
                m_FollowingPlayers[i].isFollowing = false;
                m_FollowingPlayers[i].gameObject.SetActive(false);

            }
        }
        m_CurrentFollowersCount = 0;
        HUDManager.Instance.AddCyclist(m_CurrentFollowersCount);
    }

    void MoveFollowPlayers()
    {
        for (int i = 0; i < m_StartingFollowersCount; i++)
        {
            if (m_FollowingPlayers[i].gameObject.activeSelf == true)
            {
                m_FollowingPlayers[i].isFollowing = true;
            }
        }
    }

    public void AddFollowPlayer()
    {
        for (int i = 0; i < m_FollowingPlayers.Count; i++)
        {
            if (m_FollowingPlayers[i].gameObject.activeSelf == false)
            {
                var position = m_FollowingPlayers[i].m_PlayerToFollow.transform.position;
                m_FollowingPlayers[i].transform.position = position + new Vector3(0, 0, m_FollowingPlayerOffset);
                m_FollowingPlayers[i].transform.rotation = this.transform.rotation;
                m_FollowingPlayers[i].isFollowing = true;
                m_FollowingPlayers[i].gameObject.SetActive(true);
                m_CurrentFollowersCount++;
                HUDManager.Instance.AddCyclist(m_CurrentFollowersCount);
                break;
            }
        }
    }

    public void DecreaseFollowPlayer()
    {
        if (m_CurrentFollowersCount > 0)
        {
            if (m_FollowingPlayers[m_CurrentFollowersCount - 1].gameObject.activeSelf == true)
            {
                m_FollowingPlayers[m_CurrentFollowersCount - 1].isFollowing = false;
                m_FollowingPlayers[m_CurrentFollowersCount - 1].gameObject.SetActive(false);
                var cyclist = PoolManager.Instance.Dequeue(ePoolType.DeadFollower);
                cyclist.transform.position = m_FollowingPlayers[m_CurrentFollowersCount - 1].transform.position;
                m_CurrentFollowersCount--;
                HUDManager.Instance.AddCyclist(m_CurrentFollowersCount);
                

            }
        }
        else
        {
            GameFailOnCollision();


        }
    }

    public void SetDistance()
    {
        var distance = m_CurrentSpline.CalculateLength(m_CurrentSpline.GetPointPercent(m_CurrentSpline.pointCount - 2),
        m_PlayerSpline.UnclipPercent(m_PlayerSpline.result.percent));
        var maxDistance = m_CurrentSpline.CalculateLength(m_CurrentSpline.GetPointPercent(m_CurrentSpline.pointCount - 2), 0);

        HUDManager.Instance.SetProgressBar(Gamemanager.Instance.Remaping(distance, 0, maxDistance, 1, 0));
    }
    #endregion FollowingPlayers

    #region GameStates
    private void OnGameStart()
    {
        m_RigidBodyPlayer.isKinematic = true;
        m_ColliderPlayer.enabled = true;
        m_PlayerSpline.follow = true;
        MoveFollowPlayers();

    }
    private void OnGameFail()
    {
        StartCoroutine(GameFail());
    }

    IEnumerator GameFail()
    {
        yield return new WaitForSeconds(0.2f);
        m_PlayerSpline.follow = false;
        m_RigidBodyPlayer.isKinematic = false;
        m_ColliderPlayer.enabled = false;
        yield return new WaitForSeconds(2f);
    }

    void GameFailOnCollision()
    {
        m_RigidBodyPlayer.gameObject.SetActive(false);
        var Player = PoolManager.Instance.Dequeue(ePoolType.DeadPlayer);
        Player.transform.position = m_RigidBodyPlayer.transform.position;
        Gamemanager.Instance.GameFail();
    }

    private void OnGameReset()
    {
        resetPlayerValues();
    }


    #endregion GameStates

}
