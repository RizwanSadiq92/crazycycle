﻿public enum eTagType
{
    Hurdle,
    Complete,
    Follower,
    Fail,
    Coin,
    Player,
    Ball,
    Untagged,
    Platform
}
