﻿using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    #region Data
    [SerializeField] private float m_PlayerXOffset;
    [SerializeField] private float m_SideSpeed;
    public Transform m_BicycleObject;
    public Transform m_Bicycle;
    [SerializeField] private float m_BikeSideLerpSpeed;

    [Header("Forward Speed")]
    [SerializeField] private SplineFollower m_SplineFollower;
    [SerializeField] private float m_PlayerForwardSpeed;
    #endregion Data

    private void FixedUpdate()
    {
        MovePlayer();
        if (Gamemanager.Instance.e_gamestates == eGameStates.Playing)
        {
            Player.Instance.SetDistance();
        }
    }


    #region PlayerMovement
    private void MovePlayer()
    {
        if (Gamemanager.Instance.e_gamestates == eGameStates.Playing)
        {
            var position = m_BicycleObject.localPosition;
            position.x += InputManager.Instance.DeltaDrag.x * m_SideSpeed;
            position.x = Mathf.Clamp(position.x, -m_PlayerXOffset, m_PlayerXOffset);
            m_BicycleObject.localPosition = position;

            var bikePos = m_Bicycle.localPosition;
            bikePos.x = Mathf.Lerp(bikePos.x, m_BicycleObject.localPosition.x, m_BikeSideLerpSpeed  * Time.deltaTime);
            m_Bicycle.localPosition = bikePos;
        }

        m_SplineFollower.followSpeed = m_PlayerForwardSpeed;
    }
    #endregion PlayerMovement


    public void SetPlayerSpline(SplineComputer i_SplineComputer)
    {
        m_SplineFollower.spline = i_SplineComputer;
    }
}
