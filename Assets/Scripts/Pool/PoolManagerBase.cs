﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class CustomPool {
    public ePoolType e_PoolType;
    public GameObject Object;
    //public int ToltalValue;

}
public class PoolManagerBase : MonoBehaviour
{
    #region Data
    public bool isDontDestroyOnLoad;
    [Space]
    public static PoolManagerBase Instance = null;
    [SerializeField]
    public CustomPool [] customPool;
    #endregion Data
    private void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {

            Destroy(this.gameObject);
        }
        if (isDontDestroyOnLoad)
        {
            DontDestroyOnLoad(this);
        }

    }


    public GameObject Dequeue(ePoolType poolType)
    {
        if (this.transform.GetChild(0) != null)
        {
            GameObject poolHolder = this.transform.GetChild(0).gameObject;

            for(int i=0;i<poolHolder.transform.childCount;i++)
            {
                if(poolHolder.transform.GetChild(i).name == poolType.ToString())
                {
                    if(poolHolder.transform.GetChild(i).childCount != 0)
                    {
                        for(int j =0; j< poolHolder.transform.GetChild(i).childCount; j++)
                        {
                            if (!poolHolder.transform.GetChild(i).GetChild(j).gameObject.activeSelf)
                            {
                                poolHolder.transform.GetChild(i).GetChild(j).gameObject.SetActive(true);
                                return poolHolder.transform.GetChild(i).GetChild(j).gameObject;
                            } 
                        }
                    }

                    GameObject poolObject = Instantiate(customPool[i].Object);
                    poolObject.transform.SetParent(poolHolder.transform.GetChild(i));
                    poolObject.SetActive(true);
                    return poolObject;
                }
            }

            
        }
        
        return null;
    }

    public void EnQueue(ePoolType poolType, GameObject poolObject)
    {
        GameObject poolHolder = this.transform.GetChild(0).gameObject;
        for (int i = 0; i < poolHolder.transform.childCount; i++)
        {
            if (poolHolder.transform.GetChild(i).name == poolType.ToString())
            {
                if(poolObject.transform.parent == poolHolder.transform.GetChild(i))
                {
                    poolObject.SetActive(false);
                    poolObject.transform.rotation = Quaternion.identity;
                }
                else
                {
                    poolObject.transform.SetParent(poolHolder.transform.GetChild(i).transform);
                    poolObject.SetActive(false);
                    poolObject.transform.rotation = Quaternion.identity;
                }
            }
        }

    }

}
