﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;
        
        [SerializeField] private ScreenData m_ScreenData;
        [SerializeField] private bool LogScreenData = false;

        public bool IsInputDown { get { return m_IsInputDown; } }
        private bool m_IsInputDown;

        private Vector3 m_MousePos => Input.mousePosition * m_ScreenData.Scaling;

        private Vector3 m_InputDownPos;
        private Vector3 m_LastInputPos;

        [SerializeField] private Vector2 DragSensitivity;
        public Vector2 DeltaDrag { get { return m_DeltaDrag * DragSensitivity / m_ScreenData.FinalDPI * m_ScreenData.Scaling; } }
        private Vector2 m_DeltaDrag;

        public Vector2 Drag { get { return m_Drag * DragSensitivity / m_ScreenData.FinalDPI * m_ScreenData.Scaling; } }
        private Vector2 m_Drag;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {

            Gamemanager.Instance.GameReset();
        }
    }
    private  void Awake()
        {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {

            Destroy(this.gameObject);
        }

            DontDestroyOnLoad(this);


        m_ScreenData.CalculateData(LogScreenData);
        }

        private void ResetValues()
        {
            m_DeltaDrag = m_Drag = Vector2.zero;
        }

        public static bool WasDebugKeyPressed = false;
        public void FixedUpdate()
        {
#if UNITY_EDITOR
            m_ScreenData.CalculateData();
#endif

            if (m_IsInputDown)
            {
                m_Drag = m_MousePos - m_InputDownPos;
                m_DeltaDrag = m_MousePos - m_LastInputPos;

                m_LastInputPos = m_MousePos;
            }
        }

        public void InputDown()
        {
            m_IsInputDown = true;
            m_InputDownPos = m_LastInputPos = m_MousePos;
            ResetValues();
        }

        public void InputUp()
        {
            m_IsInputDown = false;
            ResetValues();
        }

    }
