using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour
{
    #region Data
    [SerializeField] private GameObject m_Coin;
    [SerializeField] private ParticleSystem m_CollisionParticle;
    [SerializeField] private BoxCollider m_CoinCollider;
    #endregion Data
    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += ResetCoin;
    }
    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= ResetCoin;
    }

    private void ResetCoin()
    {
        m_Coin.SetActive(true);
        m_CoinCollider.enabled = true;
    }


    #region Collision
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(nameof(eTagType.Player)))
        {
            m_Coin.SetActive(false);
            m_CoinCollider.enabled = false;
            m_CollisionParticle.Play(true);
            HUDManager.Instance.AddCoins();
        }
    }

    #endregion Collision

}

