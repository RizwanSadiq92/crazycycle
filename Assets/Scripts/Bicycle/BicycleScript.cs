using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BicycleScript : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float m_ZRotationOffset;
    [SerializeField] private float m_Speed;
    private Vector3 m_InitialRotation;
    [SerializeField] private float m_LerpBackSpeed;

    [SerializeField] float turn;
    void Start()
    {

        m_InitialRotation = transform.localEulerAngles;
    }

    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += ResettoZero;

    }
    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= ResettoZero;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        RotatePlayer();
    }

    public void RotatePlayer()
    {
        if (Gamemanager.Instance.e_gamestates == eGameStates.Playing)
        {
            turn += InputManager.Instance.DeltaDrag.x;

            var rotation = transform.localEulerAngles;
            var dragSpeed = turn * m_Speed;
            rotation.z = m_InitialRotation.z + dragSpeed;


            if (turn > 0 || turn < 0)
                turn = Mathf.Lerp(turn, 0, m_LerpBackSpeed * Time.deltaTime);

            transform.localEulerAngles = rotation;

        }
    }

    public void ResettoZero()
    {
        var rotation = transform.localEulerAngles;
        rotation.z = 0;
        transform.localEulerAngles = rotation;
    }
}
