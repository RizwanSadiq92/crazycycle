﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tweenloop : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Vector3 m_OffsetRotate;
    [SerializeField] private Vector3 m_OffsetMove;
    [SerializeField] private float m_Speed;
    [SerializeField] private bool m_isrotate;
    [SerializeField] private bool m_ismove;


    void OnEnable ()
    {
        if (m_isrotate)
        {
            TweenRotateLoop();
        }
        if (m_ismove)
        {
            TweenMoveLoop();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TweenRotateLoop()
    {
        this.transform.DORotate(m_OffsetRotate,m_Speed,RotateMode.Fast).SetLoops(-1,LoopType.Yoyo);
    }

    public void TweenMoveLoop()
    {
        this.transform.DOMove(m_OffsetMove, m_Speed).SetLoops(-1, LoopType.Yoyo);
    }
}
