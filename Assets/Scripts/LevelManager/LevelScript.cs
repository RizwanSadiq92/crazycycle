using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScript : MonoBehaviour
{
    [SerializeField] private SplineComputer m_SplineComputer;
    public SplineComputer SplineComputer { get { return m_SplineComputer; } }

    [SerializeField]private int m_followersCount;
    public int FollowersCount {get { return m_followersCount; } }

}
