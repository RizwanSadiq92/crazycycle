using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    public LevelScript[] LevelScripts;
    
    public LevelScript CurrentLevel;


    private void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {

            Destroy(this.gameObject);
        }


        DontDestroyOnLoad(this);
       
    }

    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += setLevel;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= setLevel;
    }

    private void Start()
    {
        setLevel();
    }


    void setLevel()
    {
        closeAllLevels();

        var levelNo = StorageManager.Instance.CurrentlevelNo;

        while(levelNo > LevelScripts.Length -1)
        {
            levelNo -= LevelScripts.Length;
        }

        LevelScripts[levelNo].gameObject.SetActive(true);
        CurrentLevel = LevelScripts[levelNo];
        Player.Instance.SetFollowersCount(LevelScripts[levelNo].FollowersCount);
        Player.Instance.SetPlayer(CurrentLevel.SplineComputer);
    }

    void closeAllLevels()
    {
        for(int i=0; i< LevelScripts.Length; i++)
        {
            LevelScripts[i].gameObject.SetActive(false);
        }
    }

}
