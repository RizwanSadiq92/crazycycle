using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagersScript : MonoBehaviour
{
    // Start is called before the first frame update
    public static ManagersScript Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {

            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this);



    }

}
