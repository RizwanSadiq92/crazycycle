using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fragments : MonoBehaviour
{
    private Vector3 m_Position;
    private Quaternion m_Rotation;
    [SerializeField] private Rigidbody m_Rigidbody;

    [SerializeField] private float m_ImpactForce;
    // Start is called before the first frame update
    void Start()
    {
        m_Position = transform.localPosition;
        m_Rotation = transform.localRotation;
        ResetFragments();
    }

    public void Explode(Transform i_Position)
    {
        m_Rigidbody.isKinematic = false;
        m_Rigidbody.AddExplosionForce(m_ImpactForce, i_Position.position,2);
    }

    public void ResetFragments()
    {
        m_Rigidbody.isKinematic = true;
        transform.localPosition = m_Position;
        transform.localRotation = m_Rotation;
    }
}
