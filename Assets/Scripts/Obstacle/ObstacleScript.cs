﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : MonoBehaviour
{
    [SerializeField] private float m_timetoDestroy;
    [SerializeField] private ePoolType m_poolType;
    private void OnEnable()
    {
       
        GameManagerDelegate.OnGameReset += Destroy;

    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= Destroy;
    }



    void Destroy()
    {
        
        PoolManager.Instance.EnQueue(m_poolType, this.gameObject);
    }
}
