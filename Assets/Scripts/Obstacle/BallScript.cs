using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private ParticleSystem m_DustParticle;
    [SerializeField] private Rigidbody m_Rigidbody;


    private void OnEnable()
    {
        m_Rigidbody.velocity = Vector3.zero;
        m_Rigidbody.angularVelocity = Vector3.zero;
    }

    #region Collision
    private void OnCollisionEnter(Collision collision)
    {
        ContactPoint contact = collision.contacts[0];
        Vector3 pos = contact.point;
        m_DustParticle.transform.position = pos;
        m_DustParticle.Play();
        if (collision.gameObject.CompareTag(nameof(eTagType.Player)))
        {
            
            m_Rigidbody.AddExplosionForce(70,contact.point,2);
            this.gameObject.tag = nameof(eTagType.Untagged);
            Player.Instance.DecreaseFollowPlayer();
            
        }
        
    }
    #endregion Collision
}
