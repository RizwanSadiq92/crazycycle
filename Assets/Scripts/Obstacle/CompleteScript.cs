using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteScript : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private GameObject m_ConfettiPArticle1;
    [SerializeField] private GameObject m_ConfettiPArticle2;
    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += OnReset;

    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= OnReset;
    }

    void OnReset()
    {
        m_ConfettiPArticle1.SetActive(false);
        m_ConfettiPArticle2.SetActive(false);
    }
    #region Collision
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag(nameof(eTagType.Player)))
        {
            m_ConfettiPArticle1.SetActive(true);
            m_ConfettiPArticle2.SetActive(true);
            Gamemanager.Instance.GameComplete();
        }
        
    }
    #endregion Collision
}
