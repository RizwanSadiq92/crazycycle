﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallObstacleCreator : MonoBehaviour
{
    #region Data
    [SerializeField] private Transform m_PointTransform;
    [SerializeField] private int m_TimeToSpawn;
    [SerializeField] private float m_YOffset;
    #endregion Data


   

    private void SpawnObstacle()
    {
        StartCoroutine(CoroutineSpawnObstacle());
    }
    private IEnumerator CoroutineSpawnObstacle()
    {
        yield return new WaitForSeconds(m_TimeToSpawn);
        var Strikeparticle = PoolManager.Instance.Dequeue(ePoolType.StrikeParticle);
        Strikeparticle.transform.position = m_PointTransform.position;

        var BallHurdle = PoolManager.Instance.Dequeue(ePoolType.BallHurdle);
        BallHurdle.tag = nameof(eTagType.Ball);
        BallHurdle.transform.position = m_PointTransform.position + new Vector3(0, m_YOffset);

    }



    #region Collision
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(nameof(eTagType.Player)))
        {
            SpawnObstacle();
        }
    }
    #endregion Collision
}
