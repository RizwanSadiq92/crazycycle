using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierScript : MonoBehaviour
{
    #region Data
    [SerializeField] private BoxCollider m_BarrierCollider;

    [SerializeField] private Fragments[] m_AllFragments;
    [SerializeField] private Transform m_InteractPoint;

    [SerializeField] private ParticleSystem m_Explosion;
    #endregion Data
    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += ResetBarrier;
    }
    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= ResetBarrier;
    }

    private void ResetBarrier()
    {
        m_BarrierCollider.enabled = true;

        foreach(Fragments fragments in m_AllFragments)
        {
            fragments.ResetFragments();
        }
    }


    #region Collision
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(nameof(eTagType.Player)))
        {
            m_BarrierCollider.enabled = false;
            m_Explosion.Play();
            foreach (Fragments fragments in m_AllFragments)
            {
                fragments.Explode(other.transform);
            }
            Player.Instance.DecreaseFollowPlayer();
        }
    }

    #endregion Collision
}
