﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamemanager : MonoBehaviour
{
    #region Data
    public bool isDontDestroyOnLoad;
    [Space]
    public eGameStates e_gamestates = eGameStates.Idle;
    public static Gamemanager Instance = null;
    #endregion Data

    private void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {

            Destroy(this.gameObject);
        }
        if (isDontDestroyOnLoad)
        {
            DontDestroyOnLoad(this);
        }

    }

    public void Gamestart()
    {
        Gamemanager.Instance.e_gamestates = eGameStates.Playing;
        GameManagerDelegate.GameStart();
        
    }

    public void GameFail()
    {
        Gamemanager.Instance.e_gamestates = eGameStates.Fail;
        GameManagerDelegate.GameFail();
    }

    public void GameReset()
    {
        Gamemanager.Instance.e_gamestates = eGameStates.Idle;
        GameManagerDelegate.GameReset();
    }

    public void GameComplete()
    {
        Gamemanager.Instance.e_gamestates = eGameStates.Complete;
        StorageManager.Instance.CurrentlevelNo++;
        GameManagerDelegate.GameComplete();
    }

    public void GameNext()
    {
        Gamemanager.Instance.e_gamestates = eGameStates.Idle;
        GameManagerDelegate.GameNext();
    }

    public float Remaping(float i_From, float i_FromMin, float i_FromMax, float i_ToMin, float i_ToMax)
    {
        float fromAbs = i_From - i_FromMin;
        float fromMaxAbs = i_FromMax - i_FromMin;

        float normal = fromAbs / fromMaxAbs;

        float toMaxAbs = i_ToMax - i_ToMin;
        float toAbs = toMaxAbs * normal;

        float to = toAbs + i_ToMin;

        return to;
    }
}
