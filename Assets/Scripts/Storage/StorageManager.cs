using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageManager : MonoBehaviour
{
    public static StorageManager Instance = null;
    #region Data
    public int CurrentlevelNo { get { return PlayerPrefs.GetInt(nameof(CurrentlevelNo)); } set { PlayerPrefs.SetInt((nameof(CurrentlevelNo)), value);}}

    #endregion Data

    private void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {

            Destroy(this.gameObject);
        }
        

    }
}
