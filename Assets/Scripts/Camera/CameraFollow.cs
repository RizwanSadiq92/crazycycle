﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    #region Data
    [SerializeField]
     private Transform target;
    [SerializeField] Transform m_LookAtTarget;
    private Vector3 m_InitialPositionCamera;

    [SerializeField]
    private Vector3 m_OffsetPosition;

    [SerializeField]
    private Vector3 m_OffsetLookAt;

    [SerializeField]
    private Space offsetPositionSpace = Space.Self;

    [SerializeField]
    private bool lookAt = true;
    [SerializeField]
    private bool isFollowTarget;


    [SerializeField] private float m_Speed;
    [SerializeField] private float m_LookAtSpeed;
    #endregion Data

    private void OnEnable()
    {
        m_InitialPositionCamera = target.localPosition;
        GameManagerDelegate.OnGameComplete += OnGameComplete;
        GameManagerDelegate.OnGameFail += OnGameFail;
        GameManagerDelegate.OnGameReset += OnGameReset;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameComplete -= OnGameComplete;
        GameManagerDelegate.OnGameFail -= OnGameFail;
        GameManagerDelegate.OnGameReset -= OnGameReset;
    }
    private void Update()
    {
        if (isFollowTarget)
        {
            Refresh();
        }
    }

    public void Refresh()
    {
        target.localPosition = m_InitialPositionCamera + m_OffsetPosition;
        var currentPosition = target.position;
        currentPosition.y = 10;
        transform.position = Vector3.Lerp(transform.position, currentPosition, Time.deltaTime * m_Speed);

        if (lookAt)
        {
            transform.LookAt(m_LookAtTarget.position + m_OffsetLookAt);
            Vector3 relativePos = (m_LookAtTarget.position + m_OffsetLookAt) - transform.position;
            Quaternion toRotation = Quaternion.LookRotation(relativePos);
            transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, m_LookAtSpeed * Time.deltaTime);
        }
    
    }

    void resetCameraState()
    {
        isFollowTarget = true;
        transform.position = target.position;
        transform.LookAt(m_LookAtTarget.position + m_OffsetLookAt);
    }

    #region GameStates
    private void OnGameComplete()
    {
        isFollowTarget = false;
    }

    private void OnGameFail()
    {
        isFollowTarget = false;
    }
    private void OnGameReset()
    {
        Invoke(nameof(resetCameraState),0.02f);
    }
    #endregion GameStates
}
