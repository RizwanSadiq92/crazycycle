﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using TMPro;

public class HUDManager : MonoBehaviour
{
    #region Data
    public bool isDontDestroyOnLoad;
    [Space]
    public static HUDManager Instance = null;
    [SerializeField]
    private ExtendedButton m_InputButton;
    [Space]
    [Header("GamePanels")]
    [SerializeField] private GameObject m_GameCompletePanel;
    [SerializeField] private GameObject m_GameFailPanel;
    [SerializeField] private GameObject m_GameStartPanel;
    [Space]
    [Header("GamePanelButtons")]
    [SerializeField] private ExtendedButton m_StartButton;
    [SerializeField] private ExtendedButton m_ResetButton;
    [SerializeField] private ExtendedButton m_NextButton;
    [Space]
    [Header("GamePanelButtons")]
    [SerializeField] private TextMeshProUGUI m_FollowersText;
    [SerializeField] private TextMeshProUGUI m_CoinsText;

    [Space]
    [Header("ProgressBar")]

    [SerializeField] private TextMeshProUGUI m_CurrentLevelText;
    [SerializeField] private TextMeshProUGUI m_NextLevelText;
    [SerializeField] private Image m_LevelProgressImage;




    #endregion Data

    private void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {

            Destroy(this.gameObject);
        }
        if (isDontDestroyOnLoad)
        {
            DontDestroyOnLoad(this);
        }

    }
    private void OnEnable()
    {
        m_InputButton.OnDownEvent += InputDown;
        m_InputButton.OnUpEvent += InputUp;
        m_StartButton.onClick.AddListener(OnClickStartButton);
        m_ResetButton.onClick.AddListener(ClickReset);
        m_NextButton.onClick.AddListener(ClickReset);
        GameManagerDelegate.OnGameFail += ShowGameFailPanel;
        GameManagerDelegate.OnGameComplete += ShowGameCompletePanel;
        GameManagerDelegate.OnGameReset += OnClickResetButton;
    }

    private void Start()
    {
        m_CoinsText.text = PlayerPrefs.GetInt(nameof(eTagType.Coin), 0).ToString();
    }

    private void OnDisable()
    {
        m_StartButton.onClick.RemoveListener(OnClickStartButton);
        m_ResetButton.onClick.RemoveListener(ClickReset);
        m_NextButton.onClick.RemoveListener(ClickReset);
        GameManagerDelegate.OnGameFail -= ShowGameFailPanel;
        GameManagerDelegate.OnGameComplete -= ShowGameCompletePanel;
        GameManagerDelegate.OnGameReset -= OnClickResetButton;
    }

    #region InputRegion
    void InputDown(PointerEventData pointer)
    {
        InputManager.Instance.InputDown();

    }

    void InputUp(PointerEventData pointer)
    {
        InputManager.Instance.InputUp();
    }
    #endregion InputRegion


    #region GameStates

    void ShowGameFailPanel()
    {
        StartCoroutine(GameFailPanel());
    }

    IEnumerator GameFailPanel()
    {
        yield return new WaitForSeconds(1f);
        m_GameFailPanel.SetActive(true);
    }

    void ShowGameCompletePanel()
    {
        StartCoroutine(GameCompletePanel());
    }

    IEnumerator GameCompletePanel()
    {
        yield return new WaitForSeconds(1f);
        m_GameCompletePanel.SetActive(true);
    }

    void ClickReset()
    {
        Gamemanager.Instance.GameReset();
    }

    void OnClickResetButton()
    {
        StartCoroutine(ClickResetButton());
    }

    IEnumerator ClickResetButton()
    {
        yield return new WaitForSeconds(0.2f);
        if (m_GameFailPanel.activeSelf)
        {
            m_GameFailPanel.SetActive(false);
        }
        if (m_GameCompletePanel.activeSelf)
        {
            m_GameCompletePanel.SetActive(false);
        }
        yield return new WaitForSeconds(0.2f);
        m_GameStartPanel.SetActive(true);

    }


    void OnClickStartButton()
    {
        ProgressBarInitialValue();
        StartCoroutine(ClickStartButton());
    }

    IEnumerator ClickStartButton()
    {
        yield return new WaitForSeconds(0f);
        Gamemanager.Instance.Gamestart();
        m_GameStartPanel.SetActive(false);
    }
    #endregion GameStates


    #region Coins
    public void AddCoins()
    {
        int Coins = PlayerPrefs.GetInt(nameof(eTagType.Coin), 0);
        Coins++;
        PlayerPrefs.SetInt(nameof(eTagType.Coin), Coins);
        m_CoinsText.text = PlayerPrefs.GetInt(nameof(eTagType.Coin), 0).ToString();
    }
    #endregion Coins

    #region Followers Count
    public void AddCyclist(int i_CurrentCylist)
    {
        m_FollowersText.text = i_CurrentCylist + "/10";
    }
    #endregion Followers Count

    #region Progress Bar
    public void SetProgressBar(float BarVlaue)
    {
        m_LevelProgressImage.fillAmount = BarVlaue;
    }

    public void ProgressBarInitialValue()
    {
        m_LevelProgressImage.fillAmount = 0;
        m_CurrentLevelText.text = StorageManager.Instance.CurrentlevelNo.ToString();
        m_NextLevelText.text = (StorageManager.Instance.CurrentlevelNo + 1).ToString();
    }

    #endregion Progress Bar



}
